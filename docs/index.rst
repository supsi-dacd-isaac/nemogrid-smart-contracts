Nemogrid smart contracts documentation
========================================

Markets management
=========================

.. toctree::
    :maxdepth: 4

    markets_management
    devices_list_management

How the energy market works
==============================

.. toctree::
    :maxdepth: 4

    how_the_market_works

Smart contracts
==================


.. toctree::
    :maxdepth: 4

    groups_manager
    markets_manager
    devices_list
    ngt



