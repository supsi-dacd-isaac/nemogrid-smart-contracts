DevicesList
'''''''''''''''''''''''

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. autosolcontract:: DevicesList
    :noindex:
    :members: devicesData, devicesFlags, devicesIdsFlags, dsosEnabled, EnabledDSO, DisabledDSO, AddedDevice, RemovedDevice, enableDSO, disableDSO, add, remove, getId, getDSO, getDeviceFlag, getDSOEnabling