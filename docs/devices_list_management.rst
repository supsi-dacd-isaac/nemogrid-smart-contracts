List of the devices
================================

Each energy market is constituted by a period (e.g. an hour) and a couple of actors (:code:`dso` and :code:`player`), which plays it.
Each :code:`player` eligible to play a market has to be associated to a :code:`device`, practically an identification string (e.g. a serial number).
The association between :code:`player` and :code:`device` is performed by :code:`DevicesList` smart contract.
Besides, to be allowed to run a market with :code:`dso`, a :code:`player` has also to be related to :code:`dso` in :code:`DevicesList`.

Only the contract owner and the accounts of enabled :code:`dso` can insert/remove devices from the list.

An example table representing a devices list is reported below.

.. list-table::
   :widths: 34 33 33
   :header-rows: 1

   * - Player [address]
     - Device identifier [bytes32]
     - DSO [address]
   * - 0x01
     - tom_riddle
     - 0xFF
   * - 0x02
     - severus_snape
     - 0xFF
   * - 0x03
     - lucius_malfoy
     - 0xF0

