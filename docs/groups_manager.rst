GroupsManager
'''''''''''''''''''''''

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. autosolcontract:: GroupsManager
    :noindex:
    :members: token, groups, AddedGroup, addGroup, getFlag, getAddress