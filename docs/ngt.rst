NGT
'''''

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. autosolcontract:: NGT
    :noindex:
    :members: name, symbol, decimals