
var SafeMath = artifacts.require('../node_modules/openzeppelin-solidity/contracts/math/SafeMath.sol');
var DevicesList = artifacts.require('./DevicesList.sol');

module.exports = function (deployer, network, accounts) {

    deployer.deploy(SafeMath);
    deployer.link(SafeMath, DevicesList);
    deployer.deploy(DevicesList);
};
