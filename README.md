[![Documentation Status](https://readthedocs.org/projects/nemogrid-smart-contracts/badge/?version=latest)](https://nemogrid-smart-contracts.readthedocs.io/en/latest/?badge=latest)

# nemogrid-smart-contracts

The basic idea of the smart contracts is to manage energy markets. The development of blockchain-based energy markets is an aim of NEMoGrid project (http://nemogrid.eu/).

The documentation is available on [Read The Docs](https://nemogrid-smart-contracts.readthedocs.io).

# Acknowledgements
The NEMoGrid project has received funding in the framework of the joint programming initiative ERA-Net Smart Energy Systems’ focus initiative Smart Grids Plus, with support from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 646039.
The authors would like to thank the Swiss Federal Office of Energy (SFOE) and the Swiss Competence Center for Energy Research - Future Swiss Electrical Infrastructure (SCCER-FURIES), for their financial and technical support to this research work.
