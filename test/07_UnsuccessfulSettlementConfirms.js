const shouldFail = require('../node_modules/openzeppelin-test-helpers/src/shouldFail');
const time = require('../node_modules/openzeppelin-test-helpers/src/time');
const utils = require('./helpers/Utils');
const constants = require('./helpers/Constants');

require('chai')
  .use(require('chai-bignumber')(web3.BigNumber))
  .should();

const MarketsManager = artifacts.require('MarketsManager');
const NGT = artifacts.require('NGT');

// Main variables
let tsDailyMarkets, tsHourlyMarkets, idx;

// Markets contract
contract('MarketsManager', function([owner, dso, player, referee, cheater]) {

    before(async function() { await time.advanceBlock(); });

    beforeEach(async function() {
        this.timeout(600000);

        this.NGT = await NGT.new();

        this.marketsManager = await MarketsManager.new(dso, this.NGT.address);

        // Mint tokens
        await this.NGT.mint(dso, constants.DSO_TOKENS);
        await this.NGT.mint(player, constants.PLAYER_TOKENS);
        await this.NGT.mint(referee, constants.REFEREE_TOKENS);

        // Set tokens allowance
        await this.NGT.increaseAllowance(this.marketsManager.address, constants.ALLOWED_TOKENS, {from: dso});
        await this.NGT.increaseAllowance(this.marketsManager.address, constants.ALLOWED_TOKENS, {from: player});

        // Get the timestamps
        let blockInfo = await web3.eth.getBlock(await web3.eth.getBlockNumber());
        tsDailyMarkets = utils.getFirstLastTSNextDay(parseInt(blockInfo.timestamp)*1000);
        tsHourlyMarkets = utils.getFirstLastTSNextHour(parseInt(blockInfo.timestamp)*1000);
    });

    describe('Unsuccessful confirms of settlements:', function() {
        it('Try to confirm the settlement of a not existing market', async function() {
            // Open the market and not confirm it
            idx = await this.marketsManager.calcIdx(player, tsDailyMarkets.first, constants.MARKET_TYPE);

            // The cheater tries to settle
            await shouldFail.reverting(this.marketsManager.confirmSettlement(idx, constants.MAX_LOWER, {from: player}));
        });

        it('A cheater, i.e. a wallet not allowed to confirm a settlement, tries to perform a confirm', async function() {
            // Open the market
            await this.marketsManager.open(player, tsDailyMarkets.first, constants.MARKET_TYPE, referee, constants.MAX_LOWER, constants.MAX_UPPER, constants.REV_FACTOR,
                                           constants.PEN_FACTOR, constants.DSO_STAKING, constants.PLAYER_STAKING, constants.PERC_TKNS_REFEREE, {from: dso});
            idx = await this.marketsManager.calcIdx(player, tsDailyMarkets.first, constants.MARKET_TYPE);

            // Confirm the market
            await this.marketsManager.confirmOpening(idx, constants.PLAYER_STAKING, {from: player});

            // Set the time a minute after the market end
            await time.increaseTo(parseInt(await this.marketsManager.getEndTime(idx)) + 60);

            // The dso settles the market
            await this.marketsManager.settle(idx, constants.MAX_LOWER, {from: dso});

            // The cheater tries to confirm
            await shouldFail.reverting(this.marketsManager.confirmSettlement(idx, constants.MAX_LOWER, {from: cheater}));
        });

        it('Try to confirm the settlement of a market, which is not yet settled', async function() {
            // Open the market
            await this.marketsManager.open(player, tsDailyMarkets.first, constants.MARKET_TYPE, referee, constants.MAX_LOWER, constants.MAX_UPPER, constants.REV_FACTOR,
                                           constants.PEN_FACTOR, constants.DSO_STAKING, constants.PLAYER_STAKING, constants.PERC_TKNS_REFEREE, {from: dso});
            idx = await this.marketsManager.calcIdx(player, tsDailyMarkets.first, constants.MARKET_TYPE);

            // Confirm the market
            await this.marketsManager.confirmOpening(idx, constants.PLAYER_STAKING, {from: player});

            // Set the time a minute after the market end
            await time.increaseTo(parseInt(await this.marketsManager.getEndTime(idx)) + 60);

            // The player tries to confirm
            await shouldFail.reverting(this.marketsManager.confirmSettlement(idx, constants.MAX_LOWER, {from: player}));
        });
    });
});
