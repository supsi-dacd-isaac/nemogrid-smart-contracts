const time = require('../node_modules/openzeppelin-test-helpers/src/time');
const utils = require('./helpers/Utils');
const constants = require('./helpers/Constants');

require('chai')
  .use(require('chai-bignumber')(web3.BigNumber))
  .should();

const MarketsManager = artifacts.require('MarketsManager');
const NGT = artifacts.require('NGT');

// Main variables
let tsDailyMarkets, tsHourlyMarkets, idx, powerPeak, calcDsoTkns, calcPlayerTkns;

// Markets contract
contract('MarketsManager', function([owner, dso, player, referee, cheater]) {

    before(async function() { await time.advanceBlock(); });

    beforeEach(async function() {
        this.timeout(600000);

        this.NGT = await NGT.new();

        this.marketsManager = await MarketsManager.new(dso, this.NGT.address);

        // Mint tokens
        await this.NGT.mint(dso, constants.DSO_TOKENS);
        await this.NGT.mint(player, constants.PLAYER_TOKENS);
        await this.NGT.mint(referee, constants.REFEREE_TOKENS);

        // Set tokens allowance
        await this.NGT.increaseAllowance(this.marketsManager.address, constants.ALLOWED_TOKENS, {from: dso});
        await this.NGT.increaseAllowance(this.marketsManager.address, constants.ALLOWED_TOKENS, {from: player});

        // Get the timestamps
        let blockInfo = await web3.eth.getBlock(await web3.eth.getBlockNumber());
        tsDailyMarkets = utils.getFirstLastTSNextDay(parseInt(blockInfo.timestamp)*1000);
        tsHourlyMarkets = utils.getFirstLastTSNextHour(parseInt(blockInfo.timestamp)*1000);
    });

    describe('Successful settlements:', function() {
        it('PowerPeak <= LowerMaximum: The player takes all the DSO tokens (prize)', async function() {
            // Set the measured power peak
            powerPeak = constants.MAX_LOWER - 1;

            // run the market
            await this.marketsManager.open(player, tsDailyMarkets.first, constants.MARKET_TYPE, referee, constants.MAX_LOWER, constants.MAX_UPPER, constants.REV_FACTOR,
                                           constants.PEN_FACTOR, constants.DSO_STAKING, constants.PLAYER_STAKING, constants.PERC_TKNS_REFEREE, {from: dso});
            idx = await this.marketsManager.calcIdx(player, tsDailyMarkets.first, constants.MARKET_TYPE);

            await this.marketsManager.confirmOpening(idx, constants.PLAYER_STAKING, {from: player});

            await time.increaseTo(parseInt(await this.marketsManager.getEndTime(idx)) + 60);

            await this.marketsManager.settle(idx, powerPeak, {from: dso});

            await this.marketsManager.confirmSettlement(idx, powerPeak, {from: player});

            // Check the tokens balances
            parseInt(await this.NGT.balanceOf(this.marketsManager.address)).should.be.bignumber.equal(0);
            parseInt(await this.NGT.balanceOf(dso)).should.be.bignumber.equal(constants.DSO_TOKENS-constants.DSO_STAKING);
            parseInt(await this.NGT.balanceOf(player)).should.be.bignumber.equal(constants.PLAYER_TOKENS+constants.DSO_STAKING);

            // Check the tokens transferrings
            parseInt(await this.marketsManager.getTknsReleasedToDSO(idx)).should.be.bignumber.equal(0);
            parseInt(await this.marketsManager.getTknsReleasedToPlayer(idx)).should.be.bignumber.equal(constants.DSO_STAKING+constants.PLAYER_STAKING);

            // Check market result and state
            parseInt(await this.marketsManager.getResult(idx)).should.be.bignumber.equal(constants.RESULT_PRIZE);
            parseInt(await this.marketsManager.getState(idx)).should.be.bignumber.equal(constants.STATE_CLOSED);
        });

        it('LowerMaximum < PowerPeak <= UpperMaximum: The player takes a part of the DSO tokens (revenue)', async function() {
            // Set the measured power peak to 12 kW, inside the maximums interval [10 - 20] kW and the player revenue
            powerPeak = constants.MAX_LOWER + 2;
            calcDsoTkns = (powerPeak - constants.MAX_LOWER) * constants.REV_FACTOR;
            calcPlayerTkns = constants.PLAYER_TOKENS + constants.DSO_STAKING - calcDsoTkns;
            calcDsoTkns = constants.DSO_TOKENS - constants.DSO_STAKING + calcDsoTkns;

            // run the market
            await this.marketsManager.open(player, tsDailyMarkets.first, constants.MARKET_TYPE, referee, constants.MAX_LOWER, constants.MAX_UPPER, constants.REV_FACTOR,
                                           constants.PEN_FACTOR, constants.DSO_STAKING, constants.PLAYER_STAKING, constants.PERC_TKNS_REFEREE, {from: dso});
            idx = await this.marketsManager.calcIdx(player, tsDailyMarkets.first, constants.MARKET_TYPE);

            await this.marketsManager.confirmOpening(idx, constants.PLAYER_STAKING, {from: player});

            await time.increaseTo(parseInt(await this.marketsManager.getEndTime(idx)) + 60);

            await this.marketsManager.settle(idx, powerPeak, {from: dso});
            await this.marketsManager.confirmSettlement(idx, powerPeak, {from: player});

            // Check the tokens balances
            parseInt(await this.NGT.balanceOf(this.marketsManager.address)).should.be.bignumber.equal(0);
            parseInt(await this.NGT.balanceOf(dso)).should.be.bignumber.equal(calcDsoTkns);
            parseInt(await this.NGT.balanceOf(player)).should.be.bignumber.equal(calcPlayerTkns);

            // Check the tokens transferrings
            parseInt(await this.marketsManager.getTknsReleasedToDSO(idx)).should.be.bignumber.equal(calcDsoTkns - (constants.DSO_TOKENS - constants.DSO_STAKING));
            parseInt(await this.marketsManager.getTknsReleasedToPlayer(idx)).should.be.bignumber.equal(calcPlayerTkns - (constants.PLAYER_TOKENS - constants.PLAYER_STAKING));

            // Check market result and state
            parseInt(await this.marketsManager.getResult(idx)).should.be.bignumber.equal(constants.RESULT_REVENUE);
            parseInt(await this.marketsManager.getState(idx)).should.be.bignumber.equal(constants.STATE_CLOSED);
        });

        it('PowerPeak > UpperMaximum AND the player staking is enough: The DSO takes a part of the player tokens (penalty)', async function() {
            // Set the measured power peak to 23 kW and the player penalty
            powerPeak = constants.MAX_UPPER + 3;
            calcDsoTkns = (powerPeak - constants.MAX_UPPER) * constants.PEN_FACTOR;
            calcPlayerTkns = constants.PLAYER_TOKENS - calcDsoTkns;
            calcDsoTkns = constants.DSO_TOKENS + calcDsoTkns;

            // run the market
            await this.marketsManager.open(player, tsDailyMarkets.first, constants.MARKET_TYPE, referee, constants.MAX_LOWER, constants.MAX_UPPER, constants.REV_FACTOR,
                                           constants.PEN_FACTOR, constants.DSO_STAKING, constants.PLAYER_STAKING, constants.PERC_TKNS_REFEREE, {from: dso});
            idx = await this.marketsManager.calcIdx(player, tsDailyMarkets.first, constants.MARKET_TYPE);

            await this.marketsManager.confirmOpening(idx, constants.PLAYER_STAKING, {from: player});

            await time.increaseTo(parseInt(await this.marketsManager.getEndTime(idx)) + 60);

            await this.marketsManager.settle(idx, powerPeak, {from: dso});
            await this.marketsManager.confirmSettlement(idx, powerPeak, {from: player});

            // Check the tokens balances
            parseInt(await this.NGT.balanceOf(this.marketsManager.address)).should.be.bignumber.equal(0);
            parseInt(await this.NGT.balanceOf(dso)).should.be.bignumber.equal(calcDsoTkns);
            parseInt(await this.NGT.balanceOf(player)).should.be.bignumber.equal(calcPlayerTkns);

            // Check the tokens transferrings
            parseInt(await this.marketsManager.getTknsReleasedToDSO(idx)).should.be.bignumber.equal(calcDsoTkns - (constants.DSO_TOKENS - constants.DSO_STAKING));
            parseInt(await this.marketsManager.getTknsReleasedToPlayer(idx)).should.be.bignumber.equal(calcPlayerTkns - (constants.PLAYER_TOKENS - constants.PLAYER_STAKING));

            // Check market result and state
            parseInt(await this.marketsManager.getResult(idx)).should.be.bignumber.equal(constants.RESULT_PENALTY);
            parseInt(await this.marketsManager.getState(idx)).should.be.bignumber.equal(constants.STATE_CLOSED);
        });

        it('PowerPeak > UpperMaximum AND the player staking is not enough: The DSO takes all the player tokens (crash)', async function() {
            // Set the measured power peak too high for the player staking
            powerPeak = constants.MAX_UPPER + (constants.PLAYER_STAKING / constants.PEN_FACTOR) + 1;

            // run the market
            await this.marketsManager.open(player, tsDailyMarkets.first, constants.MARKET_TYPE, referee, constants.MAX_LOWER, constants.MAX_UPPER, constants.REV_FACTOR,
                                           constants.PEN_FACTOR, constants.DSO_STAKING, constants.PLAYER_STAKING, constants.PERC_TKNS_REFEREE, {from: dso});
            idx = await this.marketsManager.calcIdx(player, tsDailyMarkets.first, constants.MARKET_TYPE);

            await this.marketsManager.confirmOpening(idx, constants.PLAYER_STAKING, {from: player});

            await time.increaseTo(parseInt(await this.marketsManager.getEndTime(idx)) + 60);

            await this.marketsManager.settle(idx, powerPeak, {from: dso});
            await this.marketsManager.confirmSettlement(idx, powerPeak, {from: player});

            // Check the tokens balances
            parseInt(await this.NGT.balanceOf(this.marketsManager.address)).should.be.bignumber.equal(0);
            parseInt(await this.NGT.balanceOf(dso)).should.be.bignumber.equal(constants.DSO_TOKENS + constants.PLAYER_STAKING);
            parseInt(await this.NGT.balanceOf(player)).should.be.bignumber.equal(constants.PLAYER_TOKENS - constants.PLAYER_STAKING);

            // Check the tokens transferrings
            parseInt(await this.marketsManager.getTknsReleasedToDSO(idx)).should.be.bignumber.equal(constants.DSO_STAKING + constants.PLAYER_STAKING);
            parseInt(await this.marketsManager.getTknsReleasedToPlayer(idx)).should.be.bignumber.equal(0);

            // Check market result and state
            parseInt(await this.marketsManager.getResult(idx)).should.be.bignumber.equal(constants.RESULT_CRASH);
            parseInt(await this.marketsManager.getState(idx)).should.be.bignumber.equal(constants.STATE_CLOSED);
        });
    });
});
