const shouldFail = require('../node_modules/openzeppelin-test-helpers/src/shouldFail');
const time = require('../node_modules/openzeppelin-test-helpers/src/time');
const utils = require('./helpers/Utils');
const constants = require('./helpers/Constants');

require('chai')
  .use(require('chai-bignumber')(web3.BigNumber))
  .should();

const MarketsManager = artifacts.require('MarketsManager');
const NGT = artifacts.require('NGT');

// Main variables
let tsDailyMarkets, tsHourlyMarkets, idx;

// Markets contract
contract('MarketsManager', function([owner, dso, player, referee, cheater]) {

    before(async function() { await time.advanceBlock(); });

    beforeEach(async function() {
        this.timeout(600000);

        this.NGT = await NGT.new();

        this.marketsManager = await MarketsManager.new(dso, this.NGT.address);

        // Mint tokens
        await this.NGT.mint(dso, constants.DSO_TOKENS);
        await this.NGT.mint(player, constants.PLAYER_TOKENS);
        await this.NGT.mint(referee, constants.REFEREE_TOKENS);

        // Set tokens allowance
        await this.NGT.increaseAllowance(this.marketsManager.address, constants.ALLOWED_TOKENS, {from: dso});
        await this.NGT.increaseAllowance(this.marketsManager.address, constants.ALLOWED_TOKENS, {from: player});

        // Get the timestamps
        let blockInfo = await web3.eth.getBlock(await web3.eth.getBlockNumber());
        tsDailyMarkets = utils.getFirstLastTSNextDay(parseInt(blockInfo.timestamp)*1000);
        tsHourlyMarkets = utils.getFirstLastTSNextHour(parseInt(blockInfo.timestamp)*1000);
    });

    describe('Unsuccessful referee decisions:', function() {

        it('A cheater, i.e. a wallet different from the referee, tries to judge', async function() {
            // run the market
            await this.marketsManager.open(player, tsDailyMarkets.first, constants.MARKET_TYPE, referee, constants.MAX_LOWER, constants.MAX_UPPER, constants.REV_FACTOR,
                                           constants.PEN_FACTOR, constants.DSO_STAKING, constants.PLAYER_STAKING, constants.PERC_TKNS_REFEREE, {from: dso});
            idx = await this.marketsManager.calcIdx(player, tsDailyMarkets.first, constants.MARKET_TYPE);

            await this.marketsManager.confirmOpening(idx, constants.PLAYER_STAKING, {from: player});

            await time.increaseTo(parseInt(await this.marketsManager.getEndTime(idx)) + 60);

            await this.marketsManager.settle(idx, constants.MAX_LOWER + 5, {from: dso});

            await this.marketsManager.confirmSettlement(idx, constants.MAX_LOWER, {from: player});

            // The cheater tries to perform the decision
            await shouldFail.reverting(this.marketsManager.performRefereeDecision(idx, constants.MAX_LOWER, {from: cheater}));
        });

        it('Try to perform a referee decision without the settlement confirm', async function() {
            // run the market
            await this.marketsManager.open(player, tsDailyMarkets.first, constants.MARKET_TYPE, referee, constants.MAX_LOWER, constants.MAX_UPPER, constants.REV_FACTOR,
                                           constants.PEN_FACTOR, constants.DSO_STAKING, constants.PLAYER_STAKING, constants.PERC_TKNS_REFEREE, {from: dso});
            idx = await this.marketsManager.calcIdx(player, tsDailyMarkets.first, constants.MARKET_TYPE);

            await this.marketsManager.confirmOpening(idx, constants.PLAYER_STAKING, {from: player});

            await time.increaseTo(parseInt(await this.marketsManager.getEndTime(idx)) + 60);

            await this.marketsManager.settle(idx, constants.MAX_LOWER + 5, {from: dso});

            // The referee tries to perform its decision
            await shouldFail.reverting(this.marketsManager.performRefereeDecision(idx, constants.MAX_LOWER, {from: referee}));
        });
    });
});
