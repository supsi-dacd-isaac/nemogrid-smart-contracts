const time = require('../node_modules/openzeppelin-test-helpers/src/time');
const utils = require('./helpers/Utils');
const constants = require('./helpers/Constants');

require('chai')
  .use(require('chai-bignumber')(web3.BigNumber))
  .should();

const MarketsManager = artifacts.require('MarketsManager');
const NGT = artifacts.require('NGT');

// Main variables
let tsHourlyMarkets, tsDailyMarkets, idx;

// Markets contract
contract('MarketsManager', function([owner, dso, player, referee]) {

    before(async function() { await time.advanceBlock(); });

    beforeEach(async function() {
        this.timeout(600000);

        this.NGT = await NGT.new();

        this.marketsManager = await MarketsManager.new(dso, this.NGT.address);

        // Mint tokens
        await this.NGT.mint(dso, constants.DSO_TOKENS);
        await this.NGT.mint(player, constants.PLAYER_TOKENS);
        await this.NGT.mint(referee, constants.REFEREE_TOKENS);

        // Set tokens allowance
        await this.NGT.increaseAllowance(this.marketsManager.address, constants.ALLOWED_TOKENS, {from: dso});
        await this.NGT.increaseAllowance(this.marketsManager.address, constants.ALLOWED_TOKENS, {from: player});

        // Get the timestamps
        let blockInfo = await web3.eth.getBlock(await web3.eth.getBlockNumber());
        tsDailyMarkets = utils.getFirstLastTSNextDay(parseInt(blockInfo.timestamp)*1000);
        tsHourlyMarkets = utils.getFirstLastTSNextHour(parseInt(blockInfo.timestamp)*1000);
    });

    describe('Successful opening of daily markets:', function() {

        it('Open a market with the correct parameters', async function() {
            // Open the market
            await this.marketsManager.open(player, tsDailyMarkets.first, constants.MARKET_TYPE_DAILY, referee, constants.MAX_LOWER, constants.MAX_UPPER, constants.REV_FACTOR,
                                           constants.PEN_FACTOR, constants.DSO_STAKING, constants.PLAYER_STAKING, constants.PERC_TKNS_REFEREE, {from: dso});

            // Get the market idx from the smart contract
            idx = await this.marketsManager.calcIdx(player, tsDailyMarkets.first, constants.MARKET_TYPE_DAILY);

            // Check the existence mapping behaviour using the two identifier
            (await this.marketsManager.getFlag(idx)).should.be.equal(true);

            // Check state and result
            parseInt(await this.marketsManager.getState(idx)).should.be.bignumber.equal(constants.STATE_WAITING_CONFIRM_TO_START);
            parseInt(await this.marketsManager.getResult(idx)).should.be.bignumber.equal(constants.RESULT_NOT_DECIDED);

            // Check the tokens staking
            parseInt(await this.NGT.balanceOf(this.marketsManager.address)).should.be.bignumber.equal(constants.DSO_STAKING);
            parseInt(await this.marketsManager.getDsoStake(idx)).should.be.bignumber.equal(constants.DSO_STAKING);

            // Check player and referee
            (await this.marketsManager.getPlayer(idx)).toString(16).should.be.equal(player);
            (await this.marketsManager.getReferee(idx)).toString(16).should.be.equal(referee);

            // Check market period
            parseInt(await this.marketsManager.getStartTime(idx)).should.be.bignumber.equal(tsDailyMarkets.first);
            parseInt(await this.marketsManager.getEndTime(idx)).should.be.bignumber.equal(tsDailyMarkets.last);

            // Check maximums
            parseInt(await this.marketsManager.getLowerMaximum(idx)).should.be.bignumber.equal(constants.MAX_LOWER);
            parseInt(await this.marketsManager.getUpperMaximum(idx)).should.be.bignumber.equal(constants.MAX_UPPER);

            // Check revenue/penalty factor
            parseInt(await this.marketsManager.getRevenueFactor(idx)).should.be.bignumber.equal(constants.REV_FACTOR);
            parseInt(await this.marketsManager.getPenaltyFactor(idx)).should.be.bignumber.equal(constants.PEN_FACTOR);

            // Check revenue percentage for the referee
            (await this.marketsManager.getRevPercReferee(idx)).toString(16).should.be.bignumber.equal(constants.PERC_TKNS_REFEREE);
        });

        it('Confirm the market opening', async function() {
            // Open the market
            await this.marketsManager.open(player, tsDailyMarkets.first, constants.MARKET_TYPE_DAILY, referee, constants.MAX_LOWER, constants.MAX_UPPER, constants.REV_FACTOR,
                                           constants.PEN_FACTOR, constants.DSO_STAKING, constants.PLAYER_STAKING, constants.PERC_TKNS_REFEREE, {from: dso});
            idx = await this.marketsManager.calcIdx(player, tsDailyMarkets.first, constants.MARKET_TYPE_DAILY);

            // Confirm the market
            await this.marketsManager.confirmOpening(idx, constants.PLAYER_STAKING, {from: player});

            // Check the market state
            parseInt(await this.marketsManager.getState(idx)).should.be.bignumber.equal(constants.STATE_RUNNING);

            // Check the tokens staking
            parseInt(await this.NGT.balanceOf(this.marketsManager.address)).should.be.bignumber.equal(constants.DSO_STAKING+constants.PLAYER_STAKING);
            parseInt(await this.marketsManager.getPlayerStake(idx)).should.be.bignumber.equal(constants.PLAYER_STAKING);
        });
    });

    describe('Successful opening of hourly markets:', function() {

        it('Open a market with the correct parameters', async function() {
            // Open the market
            await this.marketsManager.open(player, tsHourlyMarkets.first, constants.MARKET_TYPE_HOURLY, referee, constants.MAX_LOWER, constants.MAX_UPPER, constants.REV_FACTOR,
                                           constants.PEN_FACTOR, constants.DSO_STAKING, constants.PLAYER_STAKING, constants.PERC_TKNS_REFEREE, {from: dso});

            // Get the market idx from the smart contract
            idx = await this.marketsManager.calcIdx(player, tsHourlyMarkets.first, constants.MARKET_TYPE_HOURLY);

            // Check the existence mapping behaviour using the two identifier
            (await this.marketsManager.getFlag(idx)).should.be.equal(true);

            // Check state and result
            parseInt(await this.marketsManager.getState(idx)).should.be.bignumber.equal(constants.STATE_WAITING_CONFIRM_TO_START);
            parseInt(await this.marketsManager.getResult(idx)).should.be.bignumber.equal(constants.RESULT_NOT_DECIDED);

            // Check the tokens staking
            parseInt(await this.NGT.balanceOf(this.marketsManager.address)).should.be.bignumber.equal(constants.DSO_STAKING);
            parseInt(await this.marketsManager.getDsoStake(idx)).should.be.bignumber.equal(constants.DSO_STAKING);

            // Check DSO and NGT
            (await this.marketsManager.getDSO()).toString(16).should.be.equal(dso);
            (await this.marketsManager.getNGT()).toString(16).should.be.equal(this.NGT.address);

            // Check player and referee
            (await this.marketsManager.getPlayer(idx)).toString(16).should.be.equal(player);
            (await this.marketsManager.getReferee(idx)).toString(16).should.be.equal(referee);

            // Check market period
            parseInt(await this.marketsManager.getStartTime(idx)).should.be.bignumber.equal(tsHourlyMarkets.first);
            parseInt(await this.marketsManager.getEndTime(idx)).should.be.bignumber.equal(tsHourlyMarkets.last);

            // Check maximums
            parseInt(await this.marketsManager.getLowerMaximum(idx)).should.be.bignumber.equal(constants.MAX_LOWER);
            parseInt(await this.marketsManager.getUpperMaximum(idx)).should.be.bignumber.equal(constants.MAX_UPPER);

            // Check revenue/penalty factor
            parseInt(await this.marketsManager.getRevenueFactor(idx)).should.be.bignumber.equal(constants.REV_FACTOR);
            parseInt(await this.marketsManager.getPenaltyFactor(idx)).should.be.bignumber.equal(constants.PEN_FACTOR);

            // Check revenue percentage for the referee
            parseInt(await this.marketsManager.getRevPercReferee(idx)).should.be.bignumber.equal(constants.PERC_TKNS_REFEREE);
        });

        it('Confirm the market opening', async function() {
            // Open the market
            await this.marketsManager.open(player, tsHourlyMarkets.first, constants.MARKET_TYPE_HOURLY, referee, constants.MAX_LOWER, constants.MAX_UPPER, constants.REV_FACTOR,
                                           constants.PEN_FACTOR, constants.DSO_STAKING, constants.PLAYER_STAKING, constants.PERC_TKNS_REFEREE, {from: dso});
            idx = await this.marketsManager.calcIdx(player, tsHourlyMarkets.first, constants.MARKET_TYPE_HOURLY);

            // Confirm the market
            await this.marketsManager.confirmOpening(idx, constants.PLAYER_STAKING, {from: player});

            // Check the market state
            parseInt(await this.marketsManager.getState(idx)).should.be.bignumber.equal(constants.STATE_RUNNING);

            // Check the tokens staking
            parseInt(await this.NGT.balanceOf(this.marketsManager.address)).should.be.bignumber.equal(constants.DSO_STAKING+constants.PLAYER_STAKING);
            parseInt(await this.marketsManager.getPlayerStake(idx)).should.be.bignumber.equal(constants.PLAYER_STAKING);
        });
    });
});
