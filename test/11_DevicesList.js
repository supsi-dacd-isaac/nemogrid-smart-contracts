const shouldFail = require('../node_modules/openzeppelin-test-helpers/src/shouldFail');
const time = require('../node_modules/openzeppelin-test-helpers/src/time');

const constants = require('./helpers/Constants');

require('chai')
  .use(require('chai-bignumber')(web3.BigNumber))
  .should();

const DevicesList = artifacts.require('DevicesList');

let addrZero = '0x0000000000000000000000000000000000000000';
const vtnIdHex = web3.utils.utf8ToHex(constants.VTN_ID);

// Markets contract
contract('DevicesList', function([owner, dso, device, cheater]) {

    before(async function() { await time.advanceBlock(); });

    beforeEach(async function() {
        this.devicesList = await DevicesList.new();
    });

    describe('DSO enablings:', function() {
        it('A not allowed address tries to enable a DSO', async function() {
            // Check the failed enabling
            await shouldFail.reverting(this.devicesList.enableDSO(dso, {from: cheater}));
            (await this.devicesList.getDSOEnabling(dso)).should.be.equal(false);
        });

        it('A DSO is successfully enabled', async function() {
            await this.devicesList.enableDSO(dso, {from: owner});

            // Check the enabling
            (await this.devicesList.getDSOEnabling(dso)).should.be.equal(true);
        });
    });

    describe('DSO disablings:', function() {
        it('A not allowed address tries to disable a DSO', async function() {
            // Enable a DSO
            await this.devicesList.enableDSO(dso, {from: owner});

            // Check the failed disabling
            await shouldFail.reverting(this.devicesList.disableDSO(dso, {from: cheater}));
            (await this.devicesList.getDSOEnabling(dso)).should.be.equal(true);
        });

        it('A DSO is successfully disabled', async function() {
            // Enable a DSO
            await this.devicesList.enableDSO(dso, {from: owner});

            // Check the enabling
            (await this.devicesList.getDSOEnabling(dso)).should.be.equal(true);

            // Disable a DSO
            await this.devicesList.disableDSO(dso, {from: owner});

            // Check the enabling
            (await this.devicesList.getDSOEnabling(dso)).should.be.equal(false);
        });
    });

    describe('Device addings:', function() {
        it('Not allowed account tries to add a device to the list', async function() {
            // Check the failed adding
            await shouldFail.reverting(this.devicesList.add(device, vtnIdHex, dso, {from: cheater}));
        });

        it('Adding device with not-meaningful data', async function() {
            // Check the failed adding
            await shouldFail.reverting(this.devicesList.add(addrZero, vtnIdHex, dso, {from: owner}));
            await shouldFail.reverting(this.devicesList.add(device, web3.utils.utf8ToHex(''), dso, {from: owner}));
            await shouldFail.reverting(this.devicesList.add(device, vtnIdHex, addrZero, {from: owner}));
            await shouldFail.reverting(this.devicesList.add(device, vtnIdHex, device, {from: owner}));
        });

        it('Adding performed on an already added device, considering the same DSO', async function() {
            // Check the failed adding
            this.devicesList.add(device, vtnIdHex, dso, {from: owner});
            await shouldFail.reverting(this.devicesList.add(device, vtnIdHex, dso, {from: owner}));
        });

        it('The owner tries to cheat adding an already inserted device', async function() {
            // Check the failed adding
            this.devicesList.add(device, vtnIdHex, dso, {from: owner});
            await shouldFail.reverting(this.devicesList.add(device, vtnIdHex, cheater, {from: owner}));
        });

        it('A cheating player tries to add an already inserted device', async function() {
            // Enable a DSO
            await this.devicesList.enableDSO(dso, {from: owner});
            await this.devicesList.enableDSO(cheater, {from: owner});

            // Check the failed adding
            this.devicesList.add(device, vtnIdHex, dso, {from: dso});
            await shouldFail.reverting(this.devicesList.add(device, vtnIdHex, cheater, {from: cheater}));
       });

        it('A cheating enabled DSO tries to add a device associated to another DSO', async function() {
            // Enable a DSO
            await this.devicesList.enableDSO(cheater, {from: owner});

            // Check the failed adding
            await shouldFail.reverting(this.devicesList.add(device, vtnIdHex, dso, {from: cheater}));
        });

        it('Successful adding performed by the owner', async function() {
            // Check the successful adding
            (await this.devicesList.getDeviceFlag(device)).should.be.equal(false);
            this.devicesList.add(device, vtnIdHex, dso, {from: owner});
            (await this.devicesList.getDeviceFlag(device)).should.be.equal(true);
        });

        it('Successful adding performed by an enabled DSO', async function() {
            // Enable a DSO
            await this.devicesList.enableDSO(dso, {from: owner});

            // Check the successful adding
            (await this.devicesList.getDeviceFlag(device)).should.be.equal(false);
            this.devicesList.add(device, vtnIdHex, dso, {from: dso});
            (await this.devicesList.getDeviceFlag(device)).should.be.equal(true);
        });
    });

    describe('Device removings:', function() {
        it('Not allowed account tries to remove a device to the list', async function() {
            // Add a device
            this.devicesList.add(device, vtnIdHex, dso, {from: owner});

            // Check the failed removing
            await shouldFail.reverting(this.devicesList.remove(device, {from: cheater}));
        });

        it('Device trying to remove itself from the list', async function() {
            // Add a device
            this.devicesList.add(device, vtnIdHex, dso, {from: owner});

            // Check the failed removing
            await shouldFail.reverting(this.devicesList.remove(device, {from: device}));
        });

        it('Device not yet added', async function() {
            // Check the failed removing
            await shouldFail.reverting(this.devicesList.remove(device, {from: owner}));
        });

        it('An enabled DSO tries to cheat removing a device of another DSO', async function() {
            // Enable two DSOs, including a cheater
            await this.devicesList.enableDSO(dso, {from: owner});
            await this.devicesList.enableDSO(cheater, {from: owner});

            // Add a device to the list
            this.devicesList.add(device, vtnIdHex, dso, {from: dso});

            // Check the failed removing performed by the enabled cheater
            await shouldFail.reverting(this.devicesList.remove(device, {from: cheater}));
        });

        it('Successful removing performed by the owner', async function() {
            // Add a device
            this.devicesList.add(device, vtnIdHex, dso, {from: owner});

            // Check the successful removing
            (await this.devicesList.getDeviceFlag(device)).should.be.equal(true);
            this.devicesList.remove(device, {from: owner});
            (await this.devicesList.getDeviceFlag(device)).should.be.equal(false);
        });

        it('Successful removing performed by an enabled DSO', async function() {
            // Enable a DSO
            await this.devicesList.enableDSO(dso, {from: owner});

            // Add a device
            this.devicesList.add(device, vtnIdHex, dso, {from: dso});

            // Check the successful removing
            (await this.devicesList.getDeviceFlag(device)).should.be.equal(true);
            this.devicesList.remove(device, {from: dso});
            (await this.devicesList.getDeviceFlag(device)).should.be.equal(false);
        });
    });
});
